#!/bin/bash

# BSD 3-Clause License
# 
# Copyright (c) 2018-2019, Alban Vidal <alban.vidal@zordhak.fr>
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
# 
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
# 
# * Neither the name of the copyright holder nor the names of its
#   contributors may be used to endorse or promote products derived from
#   this software without specific prior written permission.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

################################################################################

# Example file « /srv/backup/files/backup_www.example.com » :
#    # FQDN
#    FQDN="www.example.com"
#    # Port for ssh
#    PORT=22
#    # User for ssh
#    USER="root"
#    # Files list
#    FILES="
#    /etc/fstab
#    /etc/crontab
#    /srv/data
#    "
#    # Max day backup
#    MAX=30

################################################################################
# Path of git repository
# ./
GIT_PATH="$(realpath ${0%/*})"
################################################################################
#### Backup variables

# Backup files
BACKUP_FILES="/srv/backup/files"

# Backup directory
BACKUP_BASE="/srv/backup/data"

# For stats in end
COUNT_OK=0
COUNT_ERROR=0

# Timeout for ssh connexion
TIMEOUT=10

# To show runtime
START_SECONDS=$(date +%s)

# Do you want show « info » level sync report?
PRINT_INFO=true

################################################################################

# RSYNC default options
RSYNC_OPTIONS='-az --relative --del --links --numeric-ids'

# You can set up your own rsync options, see README to read more about that
#
# If rsync_option file exist, load this
if [ -f $GIT_PATH/rsync_option.conf ] ; then
    . $GIT_PATH/rsync_option.conf
fi

################################################################################

# SSH default options
# - StrictHostKeyChecking => auto accept ssh remote fingerprint
# - UserKnownHostsFile    => write fingerprint to /dev/null
SSH_OPTIONS='-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null'

# You can set up your own ssh options, see README to read more about that
#
# IF ssh rsync_option file exist, load this
if [ -f $GIT_PATH/ssh_option.conf ] ; then
    . $GIT_PATH/ssh_option.conf
fi

################################################################################
function LOG()
{
    # --priority warning => 4
    # --priority info => 6
    # --priority debug => 7
    PRIORITY=$1
    shift
    logger --tag backup --priority $PRIORITY "$@"

    # if priority is warning, print that
    if [ "$PRIORITY" == "warning" ]; then
        echo "BACKUP WARNING: $@"
    fi

    # If you want to show « info » level
    if [[ "$PRINT_INFO" == "true" && "$PRIORITY" == "info" ]] ;then
        echo "$@"
    fi

}

################################################################################

# Test if rsync is available
if ! which rsync 2>&1 >/dev/null ; then
    LOG err "ERROR - 'rsync' is not available, please install rsync package or verify your PATH variable"
    exit 1
fi

################################################################################
# Function to convert the seconds in hours, minuts and seconds
function fConvertSecsHMS() {
    ((h=${1}/3600))
    ((m=(${1}%3600)/60))
    ((s=${1}%60))
    printf "%02d:%02d:%02d\n" $h $m $s
}
################################################################################

# Create backup for each file of '$BACKUP_FILES' directory
for SRV in $BACKUP_FILES/*; do
    # Set start seconds
    THIS_START_SECONDS=$(date +%s)
    # Name of backup (just name of file)
    NAME=$(basename $SRV)

    # Source variables
    source $SRV

    LOG info "$NAME - Start of backup"
    
    # Where is locate backup
    DIR_BACKUP="${BACKUP_BASE}/${NAME}"
    # Create directory if not exist
    mkdir -p $DIR_BACKUP
    cd $DIR_BACKUP
    # Prevent 'rm -rf' below if incorrect
    if [ "$DIR_BACKUP" != $(pwd) ] ; then
        LOG warning "$NAME - ERROR: Can't change directory to $DIR_BACKUP"
        exit 1
    fi

    # Create rsync line
    set -f # Disable file name generation (globbing)
    BACKUP_FILES=""
    for S in $FILES ; do
        BACKUP_FILES+="$USER@${FQDN}:$S "
    done
    set +f # Re-enable file name generation (globbing)

    # Date out of range (MAX + 1)
    OLD=$(date -I -d "$(( MAX + 1 )) day ago")

    # Name of directory of yesterday and today
    YESTERDAY=$(date -I -d "1 day ago")
    TODAY=$(date -I)

    # Test is today directory already exists
    if [ -d $TODAY ]; then
        LOG debug "$NAME - Directory of today ($TODAY) exist, no sync with the past"
        FIND=true
    else
        # Search most recent directory as a reference
        LOG debug "$NAME - Directory of today ($TODAY) does NOT exist, try to sync with the past..."
        CURRENT_SCAN=1
        FIND=false
        while true ; do
            CURRENT_SCAN_DATE=$(date -I -d "$CURRENT_SCAN day ago")
            if [ -d $CURRENT_SCAN_DATE ] ; then
                LOG debug "$NAME - Directory in past exists ($CURRENT_SCAN_DATE) sync with it"
                # cp options
                # -a, --archive => same as -dR --preserve=all
                # -l, --link    => hard link files instead of copying
                cp --archive --link $CURRENT_SCAN_DATE $TODAY

                # Directory found! Exit of this loop
                FIND=true
                break
            fi

            LOG debug "$NAME - Directory in past $CURRENT_SCAN_DATE does not exist, try with next..."

            # Break if current scan is older than max backup retention
            if [ $CURRENT_SCAN -gt $MAX ] ; then
                break
            fi

            (( CURRENT_SCAN ++ ))
        done
    fi

    # if reference directory and today does not exists
    if ! $FIND ; then
        LOG debug "$NAME - No old directory exist (since $CURRENT_SCAN_DATE), create empty directory for today ($TODAY)"
        mkdir -p $TODAY
    fi

    # If OLD directory exist, delete this (out of range)
    for DIR in * ; do
        if [[ "$DIR" < "$OLD" ]] ; then
            LOG debug "$NAME - $DIR is too old, deleting"
            rm -rf $DIR
        fi
    done

    # Launch rsync and test return
    if RETURN=$(rsync $RSYNC_OPTIONS -e "ssh -p $PORT -o ConnectTimeout=$TIMEOUT $SSH_OPTIONS" $BACKUP_FILES $TODAY 2>&1) ; then
        LOG info "$NAME - Sync OK"
        (( COUNT_OK ++ ))
    else
        # If error in rsync
        LOG warning "$NAME - Sync ERROR"
        LOG warning "$NAME - $RETURN"
        (( COUNT_ERROR ++ ))
    fi

    # Seconds in end
    THIS_END_SECONDS=$(date +%s)
    # Time in seconds
    THIS_SECONDS=$(( THIS_END_SECONDS - THIS_START_SECONDS ))

    LOG info "$NAME - End of backup in $(fConvertSecsHMS $THIS_SECONDS)"
    echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"

done

# END OF SYNC
END_SECONDS=$(date +%s)
TOTAL_SECONDS=$(( END_SECONDS - START_SECONDS ))

LOG info "End of all sync in $(fConvertSecsHMS $TOTAL_SECONDS)"
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"

# End summary
if [ $COUNT_ERROR -gt 0 ]; then
    LOG warning "$COUNT_ERROR errors, $COUNT_OK OK"
    exit 1
else
    LOG info "Finish with $COUNT_OK sync OK, no errors"
fi 
