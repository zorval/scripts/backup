# backup
Backup incremental with rsync and physical links

## How to use

+ Clone this repository
```bash
git clone https://framagit.org/zorval/scripts/backup.git
```

+ Create directory data backup and backup description
```bash
mkdir -p /srv/backup/files
mkdir -p /srv/backup/data
```

+ Example of backup description files, here `/srv/backup/files/backup_www.example.com`
```bash
# FQDN
FQDN="www.example.com"
# Port for ssh
PORT=22
# User for ssh
USER="root"
# Files list
FILES="
/etc/fstab
/etc/crontab
/srv/data
/srv/lxd
"
# Max day backup
MAX=30
```

+ If necessary, edit « Backup variables » in backup.sh script

--------------------------------------------------------------------------------

## Set up your own rsync options

Here, the default rsync options are set up in `backup.sh` file, with the bellow content:

```bash
-az
--relative
--del
--links
--numeric-ids'
```

To set up your own rsync options, your need to create `rsync_option.conf` file in the root of git repository.

### Example

```bash
# Go to the git directory
cd /path/to/git/repository

# Define your own rsync options
cat << EOF > rsync_option.conf
RSYNC_OPTIONS='-az --chown=root:root --del --links --numeric-ids'
EOF
```

--------------------------------------------------------------------------------

## Set up your own ssh options

Here, the default ssh options are set up in `backup.sh` file, with the bellow content:

```bash
-o StrictHostKeyChecking=no
-o UserKnownHostsFile=/dev/null
```

To set up your own ssh options, your need to create `ssh_option.conf` file in the root of git repository.

### Example

```bash
# Go to the git directory
cd /path/to/git/repository

# Define your own ssh options
cat << EOF > ssh_option.conf
SSH_OPTIONS='-o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o IdentityFile=/path/to/private/key -o LogLevel=QUIET'
EOF
```

--------------------------------------------------------------------------------

## Example of systemd service and timer

+ Create backup service (edit « ExecStart PATH »)
```bash
cat << EOF > /etc/systemd/system/backup.service
[Unit]
Description=Backup script

[Service]
Type=oneshot
ExecStart=/srv/git/backup/backup.sh
EOF
```

+ Create backup timer (cron like)
```bash
cat << EOF > /etc/systemd/system/backup.timer
[Unit]
Description=Backup Timer

[Timer]
# Time between running each consecutive time
OnCalendar=daily

[Install]
WantedBy=timers.target
EOF
```

+ Reload daemon, enable and start the timer
```bash
systemctl daemon-reload
systemctl enable --now backup.timer
```

+ You can check timer status, and timers
```bash
systemctl status backup.timer
systemctl list-timers
```
